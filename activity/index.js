/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	/*function printWelcomeMessage () {
 	let fullName = prompt("Enter your Full Name");
 	let age = prompt("Enter your Age");
 	let address = prompt("Enter your Address");

 	console.log("Hi!" + " " + fullName + " " + age +" " + address);
}

printWelcomeMessage();*/


let fullName = prompt ("Enter your Full Name:");

 let age = prompt ("Enter your Age:");

  let address = prompt ("Enter your Address:");

 console.log ("Hello," + fullName); 
 console.log ("You are" + " " + age + " " + "years old"); 
  console.log ("You are from" + " " + address); 


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:


	{
		let band1 = "The Beatles";
		let band2 = "The Rolling Stones";
		let band3 = "Queen";
		let band4 = "Nirvana";
		let band5 = "U2";

		console.log("1." + band1)
		console.log("2." + band2)
		console.log("3." + band3)
		console.log("4." + band4)
		console.log("5." + band5)


	}
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	{
		let movie1 = "The Pursuit of Happiness";
		let movie2 = "Back to the Future";
		let movie3 = "Mission Impossible";
		let movie4 = "Top Gun: Maverick";
		let movie5 = "Titanic";

		console.log(movie1 + " " + "100%")
		console.log(movie2 + " " + "99%")
		console.log(movie3 + " " + "98%")
		console.log(movie4 + " " + "97%")
		console.log(movie5 + " " + "96%")


	}

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printFriends = function printUsers() {
	alert ("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);